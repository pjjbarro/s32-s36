const Course = require('../models/Course')

//add course
module.exports.addCourse = (req,res) => {

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})


	//always return your res.send()
	//res.send() should always be the last process done. No other steps within the code block should be done after res.send()
	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err))

}

module.exports.getAllcourses = (req,res) => {

	Course.find()
	.then(allCourses => res.send(getActiveCourses))
	.catch(error => res.send(error))
}
module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive:true})
	.then(activeCourses => res.send(activeCourses))
	.catch(error => res.send (error))
}
module.exports.getSingleCourse = (req,res) => {

	Course.findById(req.params.id)
	.then(foundCourse => res.send(foundCourse))
	.catch(error => res.send(error))
}

module.exports.updateCourse = (req,res) => {

	let UpdateCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
}
	Course.findById(req.params.id,updatedCourse,{new:true})
		.then(updatedCourse => res.send(updatedCourse))
		.catch(error => res.send(error))
}
module.exports.archieveCourse = (req,res) => {
		let UpdateCourse = {

			isActive: false
		}

		Course.findByIdAndUpdate(req.params.id,updatedCourse.{new:true})
		.then(archievedCourse => res.send(archievedCourse))
		.catch(error => res.send(error))

}

module.exports.activateCourse = (req,res) => {
	let updatedCourse = {

		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id,updatedCourse,{new:true})
	.then(activatedCourse => res.send(activatedCourse))
	.catch(error =>(error))
}
module.exports.getEnrollees = (req,res) => {

	Course.findById(req.params.id)
	.then(result=> res.send(result.enrollees))
	.catch(error=> res.send(error))
}