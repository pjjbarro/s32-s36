const User = require('../models/User');
const bcrypt = require('bcrypt');
//bcrypt has methods which will help us add a layer of security for our users' passwords.
//import the auth module and deconstruct to get our createAccessToken method.
const auth = require('../auth');
const {createAccessToken} = auth;

module.exports.registerUser = (req,res) => {

	console.log(req.body);
	if(req.body.password.length < 8) return res.send({message: "Password is too short."})
	/*
		bcrypt adds a layer of security to our passwords.

		What bcrypt does is hash our password string into a randomized character version of your password.

		It is able to hide your password within that randomized string.

		syntax:
		bcrypt.hashSync(<stringToBeHashed>,<saltRounds>)

		Salt-Rounds are the number of times the characters in the hash are randomized.

	*/
	const hashedPW = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPW);


	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW

	})
	
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err))


}

module.exports.loginUser = (req,res) => {



	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
			return res.send({message: "No User Found."})
		} else {
/*			console.log(req.body.email);
			console.log(req.body.password);
*/		
		//if we find a user, result will contain the found user's document:
		const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password);
		console.log(isPasswordCorrect);
		/*
			bcrypt.compareSync(<string>,<hashedString>)

			return a boolean after comparing the first argument (string) and the hashed version of that string. IF it matches, it will return true and it doesn't it returns false.

			IN our booking system's login, we compare the input password and the hashed password from our database.
		*/
			if(isPasswordCorrect){

				//console.log("We will generate a key.")
				return res.send({accessToken: createAccessToken(result)});
			} else {

				return res.send({message: "Password is incorrect."})

			}
		}

	})
	.catch(err => res.send(err))

}

module.exports.getSingleUser = (req,res) => {

	//logged in user's details after decoding with auth module's verify()
	console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

module.exports.updateProfile = (req,res) => {

		console.log(req.user);

		//Find the user by its id and update it
		let updates = {

			firstName: req.body.firstName,
			lastName: req.body.lastName,
			mobileNo.: req.body.mobileNo

		}

		User.findByIdAndUpdate(req.user.id,updates,{new:true})
		.then(updatedUser => res.send(updatedUser))
		.catch(err => res.send(err))

}

module.exports.updateCourse = (req,res) => {

	console.log(req.user);

	let updates = {

			firstName: req.body.firstName,
			lastName: req.body.lastName,
			price: req.body.price

	}
}

module.exports.enroll = async (req,res) => {

	//console.log(req.user.id);//gets user id from token through verify ()
	//console.log(req.body.courseId);//gets course Id from request body

	/*

		Enrollment steps:

		1. Look for our user.
				Push the details of the course we're trying to enroll in our users enrollment subdocument array. save() the user document and return true to a variable if saving is successful, return the error message if we catch an error otherwise
		2. Look for our course	
				Push the details of the course we're trying to enroll in our users enrollee's subdocument array. save() the user document and return true to a variable if saving is successful, return the error message if we catch an error otherwise	 
		3. When both saving of documents are successful, we send a message to the client

	*/

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		//check your user's enrollments subdoc array
		console.log(user.enrollments);

		//Add the courseId in the user's enrollment array:
		user.enrollments.push({courseId: req.body.courseId})

		//return the value of saving the document
		return user.save()
		.then(user => true)
		.catch(err => err.message)
	})

		console.log(isUserUpdated);
		//End the request/response when isUserUpdated does not return true
		if(isUserUpdated !== true) return res.send(isUserUpdated);

		let isCourseUpdated = await Course.findById(req.body.courseId).then(course =>{

			//add the logged in user's id into the course's enrollees subdocument array:
			course.enrollees.push({userId: req.user.id})

			return course.save()
			.then(user => true)
			.catch(err => err.message)
		})

		console.log(isCourseUpdated);
		if(isCourseUpdated !== true) return res.send(isCourseUpdated);

		if(isUserUpdated && isCourseUpdated) return res.send("Enrolled Successfully");
}		

module.exports.checkEmailExists = (req,res) => {

	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
				return res.send({
					isAvailable: true,
					message: "Email Available"
				})
		} else {
				return res.send({
					isAvailable: false,
					message: "Email is already registered".
				})
		}
	})
	.catch(err => res.send(err));
}
module.exports.getEnrollments = (req,res) => {
	Users.findById(req.user.id) 
	.then(result=> res.send(result.enrollments))
	.catch(error=> res.send(error))
}