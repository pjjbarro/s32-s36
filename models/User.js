//Activity
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "FirstName is required."]
	},
	lastName: {
		type: String,
		required: [true, "LastName is required."]
	},
	mobileNo.: {
		type: String,
		required: [true, "MobileNo. is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: true 
	}
})
module.exports = mongoose.model("User")