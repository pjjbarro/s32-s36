const express = require('express');
const router = express.Router();
const courseControllers = require('../controllers/courseControllers');
const {
	addCourse,
	getAllCourses,
	getActiveCourses,
	getSingleCourse,
	updateCourse,
	archieveCourse,
	activateCourse
} = courseControllers;
const {addCourse} = courseControllers;
const auth = require('../auth');
const {verify,verifyAdmin} = auth;
//add course
router.post('/',verify,verifyAdmin,addCourse);


router.get('/',verify,verifyAdmin,getActiveCourses)

router.get('/getActs',getActiveCourses)

router.get('.getSingleCourse/:id',getSingleCourse)


router.get('/:id',verify,verifyAdmin,updateCourse)

router.get('/archieve/:id',verify,verifyAdmin,archieveCourse)

router.get('/activate/:id',verify,verifyAdmin,activateCourse)

//Activity 5

router.get('/getEnrollees/:id',verify,verifyAdmin,getEnrollees)
module.exports = router;