const express = require('express')
const router = express.Router()
const userControllers = require('../controllers/userControllers')
const {registerUser,loginUser,getSingleUser,updateProfile,enroll} = userControllers
[
	getSingleUser,
	updateProfile,
	enroll,
	checkEmailExists,
] = userControllers
const auth = require('../auth')
//verify is a method from our auth module which will let us verify if the jwt is legitimate and let us decode the payload
const {verify} = auth;
//add user
router.post('/',registerUser);

//login user
router.post('/login',loginUser);

//getSIngleUser Details
//route methods, much like middleware, give access to req,res and next objects for the functions that are included in them.
//In fact, in ExpressJS, we can have multiple layers of middleware to do tasks before letting another function perform another task.
//Mini-Activity
router.get('/getUserDetails',verify,getSingleUser)

//update the logged in user (profile)
router.put('/updateProfile',verify,updateProfile)

//enroll route
router.post('/enroll',verify,enroll);

//check email exist route
router.post('/checkEmailExists',checkEmailExists);

//Activity 5
router.get('/getEnrollments',verify,getEnrollments);

module.exports = router;